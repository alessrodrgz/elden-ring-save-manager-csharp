﻿using System;
using System.IO;

namespace Elden_Ring_Save_Manager
{
    class Saves
    {
        public void ImportSave(string saveDir, string profileDir, string saveName, bool replace = false)
        {
            string saveFile = String.Format("{0}\\ER0000.sl2", saveDir);
            if (File.Exists(saveFile))
            {
                if (!Directory.Exists(profileDir))
                {
                    Directory.CreateDirectory(profileDir);
                }
                string newSaveFile = String.Format("{0}\\{1}.sl2", profileDir, saveName);
                if (!File.Exists(newSaveFile))
                {
                    File.Copy(saveFile, newSaveFile);
                }
                else if (replace)
                {
                    File.Delete(newSaveFile);
                    File.Copy(saveFile, newSaveFile);

                }
                else throw (new SaveAlreadyExists());
            }
            else throw(new SaveDataNotFound(saveDir));
        }
        public void DeleteSave(string profileDir, string saveName)
        {
            string saveFile = String.Format("{0}\\{1}.sl2", profileDir, saveName);
            if (File.Exists(saveFile))
            {
                File.Delete(saveFile);
            }
        }

        public void LoadSave(string saveDir, string profileDir, string saveName)
        {
            string saveFile = String.Format("{0}\\{1}.sl2", profileDir, saveName);
            if (File.Exists(saveFile))
            {
                string origSave = String.Format("{0}\\ER0000.sl2", saveDir);
                if (File.Exists(origSave))
                {
                    File.Delete(origSave);
                }
                File.Copy(saveFile, String.Format("{0}\\ER0000.sl2", saveDir));
            }
        }
    }
}
