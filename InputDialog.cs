﻿using System.Windows.Forms;

public static class InputDialog
{
    public static string ShowDialog(string text, string caption)
    {
        Form prompt = new Form()
        {
            Width = 250,
            Height = 160,
            FormBorderStyle = FormBorderStyle.FixedDialog,
            Text = caption,
            StartPosition = FormStartPosition.CenterParent,
            ControlBox = false
        };
        Label textLabel = new Label() { Left = 25, Top = 20, Text = text };
        Label errorLabel = new Label() { Left = 25, Top = 75, AutoSize = true };
        TextBox textBox = new TextBox() { Left = 25, Top = 50, Width = 175 };
        Button confirmation = new Button() { Text = "OK", Left = 25, Width = 80, Top = 95, DialogResult = DialogResult.OK};
        Button cancel = new Button() { Text = "Cancel", Left = 125, Width = 80, Top = 95, DialogResult = DialogResult.Cancel };
        confirmation.Click += (sender, e) => { prompt.Close(); };
        cancel.Click += (sender, e) => { prompt.Close(); };

        prompt.Controls.Add(textBox);
        prompt.Controls.Add(confirmation);
        prompt.Controls.Add(cancel);
        prompt.Controls.Add(textLabel);
        prompt.Controls.Add(errorLabel);
        prompt.CancelButton = cancel;
        prompt.AcceptButton = confirmation;

        return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : null;
    }
}