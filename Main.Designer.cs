﻿
using System;

namespace Elden_Ring_Save_Manager
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveLocationLabel = new System.Windows.Forms.Label();
            this.saveLocationBox = new System.Windows.Forms.TextBox();
            this.saveLocationBrowseBtn = new System.Windows.Forms.Button();
            this.saveLocationDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.editProfilesBtn = new System.Windows.Forms.Button();
            this.profileLabel = new System.Windows.Forms.Label();
            this.profilesBox = new System.Windows.Forms.ComboBox();
            this.savesBox = new System.Windows.Forms.ListBox();
            this.importBtn = new System.Windows.Forms.Button();
            this.loadBtn = new System.Windows.Forms.Button();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.successLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // saveLocationLabel
            // 
            this.saveLocationLabel.AutoSize = true;
            this.saveLocationLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.saveLocationLabel.Location = new System.Drawing.Point(13, 13);
            this.saveLocationLabel.Name = "saveLocationLabel";
            this.saveLocationLabel.Size = new System.Drawing.Size(105, 21);
            this.saveLocationLabel.TabIndex = 0;
            this.saveLocationLabel.Text = "Save location:";
            // 
            // saveLocationBox
            // 
            this.saveLocationBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.saveLocationBox.Location = new System.Drawing.Point(124, 13);
            this.saveLocationBox.Name = "saveLocationBox";
            this.saveLocationBox.ReadOnly = true;
            this.saveLocationBox.Size = new System.Drawing.Size(277, 23);
            this.saveLocationBox.TabIndex = 1;
            // 
            // saveLocationBrowseBtn
            // 
            this.saveLocationBrowseBtn.Location = new System.Drawing.Point(423, 13);
            this.saveLocationBrowseBtn.Name = "saveLocationBrowseBtn";
            this.saveLocationBrowseBtn.Size = new System.Drawing.Size(89, 23);
            this.saveLocationBrowseBtn.TabIndex = 2;
            this.saveLocationBrowseBtn.Text = "Browse";
            this.saveLocationBrowseBtn.UseVisualStyleBackColor = true;
            this.saveLocationBrowseBtn.Click += new System.EventHandler(this.saveLocationBrowseBtn_Click);
            // 
            // editProfilesBtn
            // 
            this.editProfilesBtn.Location = new System.Drawing.Point(422, 54);
            this.editProfilesBtn.Name = "editProfilesBtn";
            this.editProfilesBtn.Size = new System.Drawing.Size(90, 23);
            this.editProfilesBtn.TabIndex = 5;
            this.editProfilesBtn.Text = "Edit profiles";
            this.editProfilesBtn.UseVisualStyleBackColor = true;
            this.editProfilesBtn.Click += new System.EventHandler(this.editProfilesBtn_Click);
            // 
            // profileLabel
            // 
            this.profileLabel.AutoSize = true;
            this.profileLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.profileLabel.Location = new System.Drawing.Point(12, 54);
            this.profileLabel.Name = "profileLabel";
            this.profileLabel.Size = new System.Drawing.Size(58, 21);
            this.profileLabel.TabIndex = 3;
            this.profileLabel.Text = "Profile:";
            // 
            // profilesBox
            // 
            this.profilesBox.FormattingEnabled = true;
            this.profilesBox.Location = new System.Drawing.Point(76, 54);
            this.profilesBox.Name = "profilesBox";
            this.profilesBox.Size = new System.Drawing.Size(325, 23);
            this.profilesBox.TabIndex = 6;
            this.profilesBox.SelectedIndexChanged += new System.EventHandler(this.profilesBox_SelectedIndexChanged);
            // 
            // savesBox
            // 
            this.savesBox.FormattingEnabled = true;
            this.savesBox.ItemHeight = 15;
            this.savesBox.Location = new System.Drawing.Point(13, 105);
            this.savesBox.Name = "savesBox";
            this.savesBox.Size = new System.Drawing.Size(403, 334);
            this.savesBox.TabIndex = 7;
            this.savesBox.SelectedValueChanged += new System.EventHandler(this.savesBox_SelectedValueChanged);
            // 
            // importBtn
            // 
            this.importBtn.Enabled = false;
            this.importBtn.Location = new System.Drawing.Point(427, 186);
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(85, 30);
            this.importBtn.TabIndex = 8;
            this.importBtn.Text = "Import save";
            this.importBtn.UseVisualStyleBackColor = true;
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // loadBtn
            // 
            this.loadBtn.Enabled = false;
            this.loadBtn.Location = new System.Drawing.Point(427, 233);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(85, 30);
            this.loadBtn.TabIndex = 9;
            this.loadBtn.Text = "Load save";
            this.loadBtn.UseVisualStyleBackColor = true;
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // deleteBtn
            // 
            this.deleteBtn.Enabled = false;
            this.deleteBtn.Location = new System.Drawing.Point(427, 283);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(85, 30);
            this.deleteBtn.TabIndex = 10;
            this.deleteBtn.Text = "Delete save";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // successLabel
            // 
            this.successLabel.AutoSize = true;
            this.successLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.successLabel.ForeColor = System.Drawing.Color.Chartreuse;
            this.successLabel.Location = new System.Drawing.Point(13, 446);
            this.successLabel.Name = "successLabel";
            this.successLabel.Size = new System.Drawing.Size(0, 19);
            this.successLabel.TabIndex = 11;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 469);
            this.Controls.Add(this.successLabel);
            this.Controls.Add(this.deleteBtn);
            this.Controls.Add(this.loadBtn);
            this.Controls.Add(this.importBtn);
            this.Controls.Add(this.savesBox);
            this.Controls.Add(this.profilesBox);
            this.Controls.Add(this.editProfilesBtn);
            this.Controls.Add(this.profileLabel);
            this.Controls.Add(this.saveLocationBrowseBtn);
            this.Controls.Add(this.saveLocationBox);
            this.Controls.Add(this.saveLocationLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Text = "Elden Ring - Save Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label saveLocationLabel;
        private System.Windows.Forms.TextBox saveLocationBox;
        private System.Windows.Forms.Button saveLocationBrowseBtn;
        private System.Windows.Forms.FolderBrowserDialog saveLocationDialog;
        private System.Windows.Forms.Button editProfilesBtn;
        private System.Windows.Forms.Label profileLabel;
        private System.Windows.Forms.ComboBox profilesBox;
        private System.Windows.Forms.ListBox savesBox;
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.Button loadBtn;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Label successLabel;
    }
}

