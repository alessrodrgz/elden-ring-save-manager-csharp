﻿using System;

namespace Elden_Ring_Save_Manager
{
    class SaveDataNotFound : Exception
    {
        public SaveDataNotFound()
        {

        }

        public SaveDataNotFound(string dir)
            : base(String.Format("Elden Ring savedata not found in: {0}", dir))
        {

        }
    }

    class SaveAlreadyExists : Exception
    {
        public SaveAlreadyExists()
        {

        }
    }
}
