﻿
namespace Elden_Ring_Save_Manager
{
    partial class ErrorPrompt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.errorLabel = new System.Windows.Forms.Label();
            this.confirmation = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.errorLabel.Location = new System.Drawing.Point(12, 9);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(52, 21);
            this.errorLabel.TabIndex = 0;
            this.errorLabel.Text = "label1";
            // 
            // confirmation
            // 
            this.confirmation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.confirmation.CausesValidation = false;
            this.confirmation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.confirmation.Location = new System.Drawing.Point(72, 75);
            this.confirmation.Name = "confirmation";
            this.confirmation.Size = new System.Drawing.Size(75, 23);
            this.confirmation.TabIndex = 1;
            this.confirmation.Text = "OK";
            this.confirmation.UseVisualStyleBackColor = true;
            this.confirmation.Click += new System.EventHandler(this.confirmation_Click);
            // 
            // ErrorPrompt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(228, 110);
            this.ControlBox = false;
            this.Controls.Add(this.confirmation);
            this.Controls.Add(this.errorLabel);
            this.Name = "ErrorPrompt";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Error";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.Button confirmation;
    }
}