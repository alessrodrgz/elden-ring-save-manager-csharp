﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elden_Ring_Save_Manager
{
    public partial class ConfirmationBox : Form
    {
        public ConfirmationBox(string text)
        {
            InitializeComponent();
            textLabel.Text = text;
        }

        private void confirmation_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
