﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elden_Ring_Save_Manager
{
    public partial class Main : Form
    {
        private Config cfg = new Config("./config.json");
        private Saves saveManager = new Saves();
        private Timer timer = new Timer();

        public Main()
        {
            InitializeComponent();
            if (cfg.config.SaveDirectory != null)
            {
                saveLocationBox.Text = cfg.config.SaveDirectory;
            }
            updateProfileBox();
            updateSavesBox();

            timer.Interval = (3000);
            timer.Tick += new EventHandler(ClearSuccessLabel);
            timer.Start();
        }

        private void ClearSuccessLabel(object sender, EventArgs e) {
            successLabel.Text = String.Empty;
        }
        private void updateProfileBox()
        {
            List<string> listNames = new List<string>();
            cfg.config.Profiles.ForEach(p => listNames.Add(p.Name));
            profilesBox.DataSource = null;
            profilesBox.Items.Clear();
            profilesBox.DataSource = listNames;
            profilesBox.Update();
        }

        private void updateSavesBox()
        {
            string profileName = profilesBox.SelectedItem.ToString();
            Profile profile = cfg.config.Profiles.Find(p => p.Name == profileName);
            savesBox.DataSource = null;
            savesBox.Items.Clear();
            savesBox.DataSource = profile.Saves;
            savesBox.Update();

        }

        private void saveLocationBrowseBtn_Click(object sender, EventArgs e)
        {
            DialogResult result = saveLocationDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                cfg.config.SaveDirectory = saveLocationDialog.SelectedPath;
                cfg.updateConfig();
                saveLocationBox.Text = saveLocationDialog.SelectedPath;
            }
        }

        private void editProfilesBtn_Click(object sender, EventArgs e)
        {
            using (Profiles profiles = new Profiles())
            {
                DialogResult result = profiles.ShowDialog();
            }
            cfg.loadConfig();
            updateProfileBox();

        }

        private void profilesBox_DropDown(object sender, EventArgs e)
        {
            updateProfileBox();
        }

        private void profilesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (profilesBox.SelectedItem != null)
            {
                string profileName = profilesBox.SelectedItem.ToString();
                Profile profile = cfg.config.Profiles.Find(p => p.Name == profileName);
                if (profile.Directory != null && !string.IsNullOrWhiteSpace(saveLocationBox.Text))
                {
                    importBtn.Enabled = true;
                    updateSavesBox();
                }
                else
                {
                    importBtn.Enabled = false;
                }
            }
        }

        private void savesBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (savesBox.SelectedItem != null)
            {
                loadBtn.Enabled = true;
                deleteBtn.Enabled = true;

            }
            else
            {
                loadBtn.Enabled = false;
                deleteBtn.Enabled = false;
            }
        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            string profileName = profilesBox.SelectedItem.ToString();
            Profile profile = cfg.config.Profiles.Find(p => p.Name == profileName);
            string saveName = InputDialog.ShowDialog("Save name: ", "Save name");

            if (saveName != null && !string.IsNullOrWhiteSpace(saveName))
            {
                bool saved = false;
                try
                {
                    saveManager.ImportSave(saveLocationBox.Text, profile.Directory, saveName);
                    saved = true;
                }
                catch (SaveDataNotFound err)
                {
                    using (ErrorPrompt prompt = new ErrorPrompt(String.Format("{0}\nPlease, check your save data directory and try again.", err.Message)))
                    {
                        DialogResult result = prompt.ShowDialog();
                    }
                }
                catch (SaveAlreadyExists err)
                {
                    using (ConfirmationBox confirmationBox = new ConfirmationBox(String.Format("Save \"{0}\" already exists.\nDo you want to replace it?", saveName)))
                    {
                        DialogResult result = confirmationBox.ShowDialog();

                        if (result == DialogResult.OK)
                        {
                            saveManager.ImportSave(saveLocationBox.Text, profile.Directory, saveName, true);
                            saved = true;
                        }
                    }
                }
                finally
                {
                    if (saved)
                    {
                        if (profile.Saves == null) profile.Saves = new List<string>();
                        profile.Saves.Add(saveName);
                        cfg.updateConfig();
                    }
                    updateSavesBox();
                }
            }
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (savesBox.SelectedItem != null)
            {
                string profileName = profilesBox.SelectedItem.ToString();
                Profile profile = cfg.config.Profiles.Find(p => p.Name == profileName);
                profile.Saves.Remove(savesBox.SelectedItem.ToString());
                saveManager.DeleteSave(profile.Directory, savesBox.SelectedItem.ToString());
                cfg.updateConfig();
                updateSavesBox();
            }
        }

        private void loadBtn_Click(object sender, EventArgs e)
        {
            if (savesBox.SelectedItem != null)
            {
                string profileName = profilesBox.SelectedItem.ToString();
                Profile profile = cfg.config.Profiles.Find(p => p.Name == profileName);
                try
                {
                    saveManager.LoadSave(cfg.config.SaveDirectory, profile.Directory, savesBox.SelectedItem.ToString());
                    successLabel.Text = String.Format("Save \"{0}\" loaded successfully.", savesBox.SelectedItem.ToString());
                }
                catch (Exception err)
                {
                    using (ErrorPrompt prompt = new ErrorPrompt(err.Message))
                    {
                        DialogResult result = prompt.ShowDialog();
                    }
                }
            }
        }
    }
}
