﻿
namespace Elden_Ring_Save_Manager
{
    partial class Profiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.profileDirectoryBox = new System.Windows.Forms.TextBox();
            this.profileDirectoryLabel = new System.Windows.Forms.Label();
            this.profileDirectoryBrowseBtn = new System.Windows.Forms.Button();
            this.profilesBox = new System.Windows.Forms.ListBox();
            this.profilesLabel = new System.Windows.Forms.Label();
            this.addProfileBtn = new System.Windows.Forms.Button();
            this.deleteProfileBtn = new System.Windows.Forms.Button();
            this.profileDirectoryDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // profileDirectoryBox
            // 
            this.profileDirectoryBox.Location = new System.Drawing.Point(12, 34);
            this.profileDirectoryBox.Name = "profileDirectoryBox";
            this.profileDirectoryBox.ReadOnly = true;
            this.profileDirectoryBox.Size = new System.Drawing.Size(209, 23);
            this.profileDirectoryBox.TabIndex = 0;
            // 
            // profileDirectoryLabel
            // 
            this.profileDirectoryLabel.AutoSize = true;
            this.profileDirectoryLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.profileDirectoryLabel.Location = new System.Drawing.Point(12, 9);
            this.profileDirectoryLabel.Name = "profileDirectoryLabel";
            this.profileDirectoryLabel.Size = new System.Drawing.Size(124, 21);
            this.profileDirectoryLabel.TabIndex = 1;
            this.profileDirectoryLabel.Text = "Profile directory:";
            // 
            // profileDirectoryBrowseBtn
            // 
            this.profileDirectoryBrowseBtn.Enabled = false;
            this.profileDirectoryBrowseBtn.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.profileDirectoryBrowseBtn.Location = new System.Drawing.Point(241, 34);
            this.profileDirectoryBrowseBtn.Name = "profileDirectoryBrowseBtn";
            this.profileDirectoryBrowseBtn.Size = new System.Drawing.Size(75, 23);
            this.profileDirectoryBrowseBtn.TabIndex = 2;
            this.profileDirectoryBrowseBtn.Text = "Browse";
            this.profileDirectoryBrowseBtn.UseVisualStyleBackColor = true;
            this.profileDirectoryBrowseBtn.Click += new System.EventHandler(this.profileDirectoryBrowseBtn_Click);
            // 
            // profilesBox
            // 
            this.profilesBox.FormattingEnabled = true;
            this.profilesBox.ItemHeight = 15;
            this.profilesBox.Location = new System.Drawing.Point(12, 108);
            this.profilesBox.Name = "profilesBox";
            this.profilesBox.Size = new System.Drawing.Size(320, 214);
            this.profilesBox.TabIndex = 3;
            this.profilesBox.SelectedIndexChanged += new System.EventHandler(this.profilesBox_SelectedIndexChanged);
            // 
            // profilesLabel
            // 
            this.profilesLabel.AutoSize = true;
            this.profilesLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.profilesLabel.Location = new System.Drawing.Point(12, 75);
            this.profilesLabel.Name = "profilesLabel";
            this.profilesLabel.Size = new System.Drawing.Size(65, 21);
            this.profilesLabel.TabIndex = 4;
            this.profilesLabel.Text = "Profiles:";
            // 
            // addProfileBtn
            // 
            this.addProfileBtn.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.addProfileBtn.Location = new System.Drawing.Point(35, 328);
            this.addProfileBtn.Name = "addProfileBtn";
            this.addProfileBtn.Size = new System.Drawing.Size(111, 33);
            this.addProfileBtn.TabIndex = 5;
            this.addProfileBtn.Text = "Add profile";
            this.addProfileBtn.UseVisualStyleBackColor = true;
            this.addProfileBtn.Click += new System.EventHandler(this.addProfileBtn_Click);
            // 
            // deleteProfileBtn
            // 
            this.deleteProfileBtn.Enabled = false;
            this.deleteProfileBtn.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.deleteProfileBtn.Location = new System.Drawing.Point(196, 328);
            this.deleteProfileBtn.Name = "deleteProfileBtn";
            this.deleteProfileBtn.Size = new System.Drawing.Size(111, 33);
            this.deleteProfileBtn.TabIndex = 6;
            this.deleteProfileBtn.Text = "Delete profile";
            this.deleteProfileBtn.UseVisualStyleBackColor = true;
            this.deleteProfileBtn.Click += new System.EventHandler(this.deleteProfileBtn_Click);
            // 
            // Profiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 370);
            this.Controls.Add(this.deleteProfileBtn);
            this.Controls.Add(this.addProfileBtn);
            this.Controls.Add(this.profilesLabel);
            this.Controls.Add(this.profilesBox);
            this.Controls.Add(this.profileDirectoryBrowseBtn);
            this.Controls.Add(this.profileDirectoryLabel);
            this.Controls.Add(this.profileDirectoryBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Profiles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Profiles";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox profileDirectoryBox;
        private System.Windows.Forms.Label profileDirectoryLabel;
        private System.Windows.Forms.Button profileDirectoryBrowseBtn;
        private System.Windows.Forms.ListBox profilesBox;
        private System.Windows.Forms.Label profilesLabel;
        private System.Windows.Forms.Button addProfileBtn;
        private System.Windows.Forms.Button deleteProfileBtn;
        private System.Windows.Forms.FolderBrowserDialog profileDirectoryDialog;
    }
}