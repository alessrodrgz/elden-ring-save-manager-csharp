﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Elden_Ring_Save_Manager
{
    class Profile
    {
        public string Name { get; set; }
        public List<string> Saves { get; set; }
        public string Directory { get; set; }
    }
    class ConfigFile
    {
        public string SaveDirectory { get; set; }
        public List<Profile> Profiles { get; set; }

    }

    class Config
    {
        public ConfigFile config;
        public string filename;
        public Config(string filename)
        {
            this.filename = filename;

            if (!File.Exists(filename))
            {
                using FileStream createStream = File.Create(filename);
                StreamWriter writer = new StreamWriter(createStream);
                writer.Write("{}");
                writer.Close();
            }

            loadConfig();

        }

        public void loadConfig()
        {
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                this.config = JsonSerializer.Deserialize<ConfigFile>(json);
                r.Close();
            }

            if (this.config.Profiles == null)
            {
                this.config.Profiles = new List<Profile>();
                updateConfig();
            }

        }

        public void updateConfig()
        {
            using (StreamWriter writer = new StreamWriter(filename))
            {
                writer.Write(JsonSerializer.Serialize(this.config));
                writer.Close();
            }
        }
    }
}
