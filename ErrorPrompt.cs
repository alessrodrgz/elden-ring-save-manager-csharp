﻿
using System.Drawing;
using System.Windows.Forms;

namespace Elden_Ring_Save_Manager
{
    public partial class ErrorPrompt : Form
    {
        public ErrorPrompt(string error)
        {
            InitializeComponent();
            errorLabel.Text = error;
            ShowIcon = true;
            Icon = SystemIcons.Error;
        }

        private void confirmation_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
