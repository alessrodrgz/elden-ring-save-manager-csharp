﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Elden_Ring_Save_Manager
{
    public partial class Profiles : Form
    {
        private Config cfg;

        public Profiles()
        {
            this.cfg = new Config("./config.json");
            InitializeComponent();
            updateProfileBox();
        }

        private void updateProfileBox()
        {
            List<string> listNames = new List<string>();
            cfg.config.Profiles.ForEach(p => listNames.Add(p.Name));
            profilesBox.DataSource = null;
            profilesBox.Items.Clear();
            profilesBox.DataSource = listNames;
            profilesBox.Update();
        }
        private void addProfileBtn_Click(object sender, System.EventArgs e)
        {
            string profileName = InputDialog.ShowDialog("Profile name: ", "Profile name");

            if (profileName != null & !string.IsNullOrWhiteSpace(profileName))
            {
                profileName = profileName.Trim();
                Profile profile = new Profile();
                profile.Name = profileName;
                cfg.config.Profiles.Add(profile);
                cfg.updateConfig();
                updateProfileBox();
            }
        }

        private void profilesBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if(profilesBox.SelectedItem != null)
            {
                profileDirectoryBrowseBtn.Enabled = true;
                deleteProfileBtn.Enabled = true;
                string profileName = profilesBox.SelectedItem.ToString();
                Profile profile = cfg.config.Profiles.Find(p => p.Name == profileName);

                if(profile.Directory != null)
                {
                    profileDirectoryBox.Text = profile.Directory;
                } else
                {
                    profileDirectoryBox.Text = "";
                }
            } else
            {
                profileDirectoryBrowseBtn.Enabled = false;
                deleteProfileBtn.Enabled = false;
            }
        }

        private void deleteProfileBtn_Click(object sender, System.EventArgs e)
        {
            if(profilesBox.SelectedItem != null)
            {
                string profileName = profilesBox.SelectedItem.ToString();
                Profile profile = cfg.config.Profiles.Find(p => p.Name == profileName);
                cfg.config.Profiles.Remove(profile);
                cfg.updateConfig();
                updateProfileBox();
            }
        }

        private void profileDirectoryBrowseBtn_Click(object sender, System.EventArgs e)
        {
            if(profilesBox.SelectedItem != null)
            {
                string profileName = profilesBox.SelectedItem.ToString();
                int profileIndex = cfg.config.Profiles.IndexOf(cfg.config.Profiles.Find(p => p.Name == profileName));

                DialogResult result = profileDirectoryDialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    cfg.config.Profiles[profileIndex].Directory = profileDirectoryDialog.SelectedPath;
                    cfg.updateConfig();
                    profileDirectoryBox.Text = profileDirectoryDialog.SelectedPath;
                }
            }
        }
    }
}
